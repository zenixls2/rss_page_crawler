export GOPATH=`pwd`/../gocode
go get github.com/spaolacci/murmur3
go get github.com/spacemonkeygo/openssl
go get gopkg.in/redis.v2
go build
cd fs/ && go build
