package main

import (
	"bufio"
	"fmt"
	"github.com/spacemonkeygo/openssl"
	"net/http"
	"os"
	"path"
	"runtime"
	"time"
)

var TIMEOUT = time.Duration(time.Second)

func ServeFile(w http.ResponseWriter, r *http.Request) {
	s := path.Join("../o", r.URL.Path[1:])
	f, e := os.Open(s)
	if e != nil {
		return
	}
	reader := bufio.NewReader(f)
	line, _, err := reader.ReadLine()
	if err != nil {
		return
	}
	w.Write(line)
	f.Close()
}

type StatusWriter struct {
	http.ResponseWriter
	status int
	length int
}

func (w *StatusWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func (w *StatusWriter) Write(b []byte) (int, error) {
	if w.status == 0 {
		w.status = 200
	}
	w.length = len(b)
	return w.ResponseWriter.Write(b)
}

type HttpsHandler struct{}

func (h *HttpsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	r.URL.Scheme = "https"
	w.Header().Set("Connection", "close")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	writer := StatusWriter{w, 0, 0}
	http.DefaultServeMux.ServeHTTP(&writer, r)
}

type HttpHandler struct{}

func (h *HttpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	r.URL.Scheme = "http"
	w.Header().Set("Connection", "close")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	writer := StatusWriter{w, 0, 0}
	http.DefaultServeMux.ServeHTTP(&writer, r)
}
func main() {
	runtime.GOMAXPROCS(1024)

	http.HandleFunc("/", ServeFile)

	ctx, _ := openssl.NewCtxFromFiles("apx.pem", "apx.key")
	ctx.ClearOptions(openssl.NoSSLv3)
	ctx.SetOptions(openssl.NoCompression)
	ctx.SetOptions(openssl.NoSSLv2)
	ctx.SetSessionCacheMode(openssl.SessionCacheOff)
	ctx.SetOptions(openssl.NoTicket)
	ctx.SetOptions(openssl.NoSessionResumptionOrRenegotiation)
	ctx.SetOptions(openssl.CipherServerPreference)
	ctx.SetEllipticCurve(openssl.Prime256v1)
	err := ctx.SetCipherList("EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4-SHA !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS")
	if err != nil {
		fmt.Println(err)
	}
	go (&http.Server{Addr: ":80", Handler: &HttpHandler{}, ReadTimeout: TIMEOUT, WriteTimeout: TIMEOUT}).ListenAndServe()
	l, _ := openssl.Listen("tcp", ":443", ctx)
	err = (&http.Server{Addr: ":443", Handler: &HttpsHandler{}, ReadTimeout: TIMEOUT, WriteTimeout: TIMEOUT}).Serve(l)
	if err != nil {
		fmt.Println(err)
	}
}
