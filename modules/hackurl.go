package module

import (
	"fmt"
	u "net/url"
	"strings"
)

var cw = "www.cw.com.tw"
var mcw = "m.cw.com.tw"
var ocw = "opinion.cw.com.tw"
var ecw = "english.cw.com.tw"
var vcw = "video.cw.com.tw"
var chealth = "www.commonhealth.com.tw"
var tchealth = "topic.commonhealth.com.tw"
var cool3c = "www.cool3c.com"
var thenewslens = "www.thenewslens.com"
var appledaily = "www.appledaily.com.tw"
var mappledaily = "m.home.appledaily.com.tw"
var mamaclub = "mamaclub.com"
var ettoday = "www.ettoday.net"
var gettoday = "game.ettoday.net"
var tettoday = "travel.ettoday.net"
var mai91 = "tw.91mai.com"
var edhealth = "www.everydayhealth.com.tw"

func TrimPath(path string, start int) string {
	if len(path) == 0 {
		return path
	}
	e := len(path) - 1
	end := e
	for ; path[e] == '/' && e > 0; e-- {
		end = e
	}
	if path[end] == '/' {
		return path[start:end]
	} else {
		return path[start:]
	}
}
func Hackurl(url string) string {
	uri, err := u.Parse(url)
	if err != nil {
		return ""
	}
	switch uri.Host {
	case cw, mcw, vcw:
		if len(uri.RawQuery) >= 3 {
			return fmt.Sprintf("cw:%s", uri.RawQuery[3:])
		} else {
			p := TrimPath(uri.Path, 0)
			if len(p) > 3 && p[len(p)-4:] == ".htm" {
				p = p[0 : len(p)-4]
			}
			parray := strings.Split(p, "/")
			return fmt.Sprintf("cw:%s", parray[len(parray)-1])
		}
	case ecw:
		if len(uri.RawQuery) > 0 {
			return fmt.Sprintf("ecw:%s", strings.Split(uri.RawQuery, "&")[1][3:])
		}
		return ""
	case ocw:
		parray := strings.Split(uri.Path, "/")
		return fmt.Sprintf("ocw:%s.%s", parray[3], parray[5])
	case chealth:
		if len(uri.RawQuery) >= 4 {
			return fmt.Sprintf("cheath:%s", uri.RawQuery[4:])
		}
		return ""
	case tchealth:
		return ""
	case cool3c:
		return fmt.Sprintf("cool3c:%s", TrimPath(uri.Path, 9))
	case thenewslens:
		return fmt.Sprintf("thenewslens:%s", TrimPath(uri.Path, 6))
	case appledaily, mappledaily:
		p := TrimPath(uri.Path, 0)
		parray := strings.Split(p, "/")
		if len(parray) >= 6 {
			return fmt.Sprintf("appledaily:%s", strings.Join(parray[4:6], "."))
		} else {
			return fmt.Sprintf("appledaily:%s", p)
		}
	case mamaclub:
		return fmt.Sprintf("mamaclub:%s", TrimPath(uri.Path, 7))
	case ettoday:
		p := TrimPath(uri.Path, 0)
		if len(p) > 3 && p[len(p)-4:] == ".htm" {
			p = p[0 : len(p)-4]
		}
		parray := strings.Split(p, "/")
		if len(parray) >= 2 {
			return fmt.Sprintf("ettoday:%s", strings.Join(parray[len(parray)-2:], "."))
		} else {
			return fmt.Sprintf("ettoday:%s", p)
		}
	case gettoday, tettoday:
		p := TrimPath(uri.Path, 0)
		if len(p) > 3 && p[len(p)-4:] == ".htm" {
			p = p[0 : len(p)-4]
		}
		parray := strings.Split(p, "/")
		if len(parray) >= 2 {
			return fmt.Sprintf("gettoday:%s", parray[len(parray)-1])
		} else {
			return fmt.Sprintf("gettoday:%s", p)
		}
	case mai91:
		parray := strings.Split(uri.Path, "/")
		if len(uri.RawQuery) >= 4 {
			return fmt.Sprintf("mai91:%s.%s", parray[len(parray)-1], uri.RawQuery[4:])
		}
		return fmt.Sprintf("mai91:%s", parray[len(parray)-1])
	case edhealth:
		return fmt.Sprintf("everydayhealth:%s", strings.Split(uri.Path, "/")[2])
	}
	return ""
}
