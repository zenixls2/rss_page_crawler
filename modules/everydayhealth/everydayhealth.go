package everydayhealth

import (
	"../../modules"
	"bufio"
	"fmt"
	u "net/url"
	"os"
	"strconv"
)

var tlog *os.File

func run(currentTime string) {
	url := "http://www.everydayhealth.com.tw"
	tlog, err := os.OpenFile(fmt.Sprintf("tlog/%s", currentTime), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	defer tlog.Close()
	file, err := os.OpenFile(fmt.Sprintf("time/%s", u.QueryEscape(url)), os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	line, _, err := reader.ReadLine()
	file.Close()
	if err != nil {
		line = []byte("1895")
	}
	index, err := strconv.Atoi(string(line))
	if err != nil {
		index = 1895
	}
	end := index
	for i := index; i < index+200; i++ {
		if module.Transaction(fmt.Sprintf(url+"/article/%d", i), tlog) {
			end = i
		}
	}
	tlog.Close()
	file, err = os.OpenFile(fmt.Sprintf("time/%s", u.QueryEscape(url)), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	_, err = file.WriteString(fmt.Sprintf("%d", end))
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	file.Close()
}
func init() {
	module.Get()["everydayhealth"] = run
}
