package module

import (
	"crypto/tls"
	"fmt"
	"golang.org/x/net/html"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
)

var modules map[string]func(string)

func dailTimeout(network, addr string) (net.Conn, error) {
	return net.DialTimeout(network, addr, time.Duration(10*time.Second))
}

var config = &tls.Config{MinVersion: tls.VersionSSL30, PreferServerCipherSuites: true, InsecureSkipVerify: true}
var client = &http.Client{
	Transport: &http.Transport{
		TLSClientConfig: config,
		Dial:            dailTimeout,
	},
}

func Curl(url string) (string, string) {
	resp, err := client.Get(url)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return url, ""
	}
	switch resp.StatusCode {
	case http.StatusMovedPermanently, http.StatusFound, http.StatusSeeOther, http.StatusTemporaryRedirect, http.StatusOK:
	default:
		//fmt.Fprintln(os.Stderr, "fatal status", resp.Status)
		//fmt.Fprintln(os.Stderr, err)
		return resp.Request.URL.String(), ""
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return resp.Request.URL.String(), ""
	}
	return resp.Request.URL.String(), string(body)
}

func GetMeta(n *html.Node, result *[]string) {
	if n.Type == html.ElementNode && n.Data == "meta" {
		var keywords string
		toappend := false
		for _, meta := range n.Attr {
			if strings.EqualFold(meta.Key, "content") {
				keywords = meta.Val
			}
			if strings.EqualFold(meta.Key, "name") {
				if strings.EqualFold(meta.Val, "keywords") || strings.EqualFold(meta.Val, "shareaholic:keywords") || strings.EqualFold(meta.Val, "news_keywords") {
					toappend = true
				}
			}
		}
		if toappend {
			*result = append(*result, keywords)
		}
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		GetMeta(c, result)
	}
}

func Transaction(url string, tlog *os.File) bool {
	link, body := Curl(url)
	if len(body) > 0 {
		doc, err := html.Parse(strings.NewReader(body))
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			return false
		}
		var result = []string{}
		GetMeta(doc, &result)
		key := Hackurl(link)
		if len(key) == 0 {
			fmt.Println(link)
			return false
		}
		_, err = tlog.WriteString(fmt.Sprintf("%s\n", key))
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			return false
		}
		file, err := os.OpenFile(fmt.Sprintf("o/%s", key), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			return false
		}
		defer file.Close()
		str := strings.Join(result, ",")
		rarray := regexp.MustCompile("(#|, |,)").Split(str, -1)
		var e = []string{}
		for _, v := range rarray {
			if len(v) > 0 {
				e = append(e, v)
			}
		}
		file.WriteString(strings.Join(e, ","))
		file.Close()
		return true
	}
	return false
}

func init() {
	modules = make(map[string]func(string))
}

func Get() map[string]func(string) {
	return modules
}
