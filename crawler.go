package main

import (
	"./modules"
	_ "./modules/everydayhealth"
	"bufio"
	"encoding/xml"
	"fmt"
	"io"
	u "net/url"
	"os"
	"time"
)

var tlog *os.File

func ParseTime(ts string) time.Time {
	t, err := time.Parse(time.RFC1123Z, ts)
	if err == nil {
		return t
	}
	err = nil
	t, err = time.Parse(time.RFC1123, ts)
	if err == nil {
		return t
	}
	err = nil
	t, err = time.Parse("2006-01-02 15:04:05.0", ts)
	if err == nil {
		return t
	}
	err = nil
	t, err = time.Parse("Mon,02 Jan 2006 15:04:05  -0700", ts)
	if err == nil {
		return t
	}
	err = nil
	t, err = time.Parse("Mon, 2 Jan 2006 15:04:05 -0700", ts)
	if err == nil {
		return t
	}
	err = nil
	t, err = time.Parse("2006-01-02 15:04:05 -0700", ts)
	if err == nil {
		return t
	}
	err = nil
	t, err = time.Parse(time.RFC3339, ts)
	if err == nil {
		return t
	}
	err = nil
	t, err = time.Parse(time.RFC3339Nano, ts)
	if err == nil {
		return t
	}
	panic(err)
}
func RssPage(url string) {
	_, body := module.Curl(url)
	v := Rss{}
	if len(body) > 0 {
		err := xml.Unmarshal([]byte(body), &v)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			return
		}
	}
	file, err := os.OpenFile(fmt.Sprintf("time/%s", u.QueryEscape(url)), os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	defer file.Close()
	reader := bufio.NewReader(file)
	line, _, err := reader.ReadLine()
	if err != nil {
		line = []byte("Thu, 01 Jan 1970 00:00:00 +0000")
	}
	file.Close()
	baseTime := ParseTime(string(line))
	compTime := baseTime
	updateTime := string(line)

	for _, item := range v.Items {
		t := ParseTime(item.PubDate)
		if baseTime.UTC().Unix() < t.UTC().Unix() {
			if item.Guid.PermaLink == "true" || item.Guid.PermaLink == "" {
				module.Transaction(item.Guid.Link, tlog)
			} else if item.Guid.PermaLink == "false" {
				_, err := u.ParseRequestURI(item.Guid.Link)
				if err != nil {
					module.Transaction(item.Link, tlog)
					// assume comments and article in the same page
					// Transaction(item.Comments)
				} else {
					module.Transaction(item.Guid.Link, tlog)
				}
			}
			if compTime.UTC().Unix() < t.UTC().Unix() {
				compTime = t
				updateTime = item.PubDate
			}
		}
	}
	file, err = os.OpenFile(fmt.Sprintf("time/%s", u.QueryEscape(url)), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	_, err = file.WriteString(updateTime)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	file.Close()
}
func main() {
	currentTime := time.Now().Format("20060102150405")
	// brute force (defined by each module)
	modules := module.Get()
	for key, value := range modules {
		fmt.Println("Running " + key)
		value(currentTime)
	}
	// rss parsing flow
	file, err := os.Open("rssurl.conf")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	tlog, err = os.OpenFile(fmt.Sprintf("tlog/%s", currentTime), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	defer tlog.Close()
	reader := bufio.NewReader(file)
	err = nil
	var line []byte
	for err == nil {
		line, _, err = reader.ReadLine()
		_, uerr := u.ParseRequestURI(string(line))
		if uerr == nil {
			RssPage(string(line))
		}
	}
	if err != nil && err != io.EOF {
		fmt.Fprintln(os.Stderr, err)
	}
	file.Close()

	tlog.Close()
}
