package main

type GuidType struct {
	Link      string `xml:",chardata"`
	PermaLink string `xml:"isPermaLink,attr,omitempty"`
}
type Item struct {
	PubDate  string   `xml:"pubDate"`
	Guid     GuidType `xml:"guid"`
	Comments string   `xml:"comments"`
	Link     string   `xml:"link"`
}
type Rss struct {
	Items []Item `xml:"channel>item"`
}
